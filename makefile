# Paths and flags
GLOBALFLAGS = -Wall -O3 -mwindows
CFLAGS = $(GLOBALFLAGS)
CXXFLAGS = $(GLOBALFLAGS)
LDFLAGS = -s -static -mwindows
LDLIBS = 
CC=gcc
CXX=g++
LD=g++
NAMES = $(filter %.h %.c %.cpp %.hpp,$(wildcard *))
FOLDERS = 

FOLDERS = bin bin/Release obj obj/Release

.PHONY: all release debug
all: release 

release: bin/Release obj/Release bin/Release/DMedia.exe

debug: bin/Debug obj/Debug bin/Debug/DMedia.exe

bin/Release bin/Debug: bin

obj/Release obj/Debug: obj

bin/Release/DMedia.exe: obj/Release/atkmediainterceptor.o obj/Release/resource.o
	$(LD) $(LDFLAGS) -o $@ $^ $(LDLIBS)

obj/Release/atkmediainterceptor.o: src/atkmediainterceptor.c
	$(CC) -c $(CFLAGS) -o $@ $^

obj/Release/resource.o: src/resource.rc
	windres $< -o $@

%.exe: %.o
	$(LD) $(LDFLAGS) -o $@ $^ $(LDLIBS)
	
$(FOLDERS):
	-mkdir "$@"
